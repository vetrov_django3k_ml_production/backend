from django import forms
from multiupload.fields import MultiFileField


class MultiFileUploadWidget(forms.widgets.FileInput):
    allow_multiple_selected = True


class BloodCheckForm(forms.Form):
    photos_date = forms.DateField(label='Photos date',
                                  required=True,
                                  widget=forms.widgets.DateInput(
                                      attrs={
                                          'type': 'date', 'placeholder': 'dd-mm-yyyy',
                                          'class': 'form-control'
                                      }
                                  ))
    uploaded_files = MultiFileField(label="Upload photos",
                                    required=True,
                                    widget=MultiFileUploadWidget(
                                        attrs={
                                            'class': 'form-control',
                                            'multiple': True
                                        },
                                    ))
