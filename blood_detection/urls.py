from django.urls import path

from .views import send_photos, index

urlpatterns = [
    path('', index, name='index'),
    path('send_photos/', send_photos, name='send_photos'),
]
