import base64
import uuid
from typing import Dict

import requests
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.shortcuts import render, redirect

from blood_detection.forms import BloodCheckForm


# Create your views here.
def index(request):
    return redirect('send_photos')


def send_photos(request):
    form = BloodCheckForm(request.POST, request.FILES)
    if form.is_valid():
        files = [('uploaded_files', (file_obj.name, file_obj.read())) for file_obj in
                 form.files.getlist('uploaded_files')]
        response = requests.post('http://127.0.0.1:5000/detect', files=files)
        if response.status_code == 200:
            result_data = response.json()
            labels: Dict = result_data['labels']
            new_labels = [label for label in labels.values()]
            new_labels[0] = new_labels[0]
            encoded_images = result_data['images']

            # save decoded images
            saved_images = []
            for encoded_image in encoded_images:
                image_data = base64.b64decode(encoded_image)
                file_name = str(uuid.uuid4()) + '.jpg'
                file_path = default_storage.save(file_name, ContentFile(image_data))
                saved_images.append(settings.MEDIA_URL + file_path)

            return render(request, 'blood_detection/results.html',
                          {
                              'images_and_labels': zip(saved_images, new_labels),
                              'report_date': form.cleaned_data['photos_date']
                          })
        else:
            return render(request, 'blood_detection/error.html')
    return render(request,
                  'blood_detection/send_photos.html',
                  context={'form': form})
